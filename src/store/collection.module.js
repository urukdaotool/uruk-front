import CollectionService from "@/services/collection.service";


export const collection = {
    namespaced: true,
    state: {
        collection: null
    },

    getters: {
        collection: (state) => { return state.collection }
    }, 
    mutations: {
        setCollection(state, collection) {
            state.collection = collection
        }
    },
    actions: {
        async getCollection({ commit }) {
            const response = await CollectionService.getCollection();
            if(response) {
                commit("setCollection", response.collection);
            }

            return Promise.resolve(response.collection)
            
        }
    }
}