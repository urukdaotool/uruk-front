import daoService from '../services/dao.service';
import DaoService from '../services/dao.service';

const daoUser = JSON.parse(localStorage.getItem('dao'));

const initialState = daoUser ? { daoName: daoUser.name, dao: daoUser, daoUpdated: false, daoImgUrl: daoUser.url, pdfCreated: false, daoPdf: [] } : { daoName: '', dao: null, daoUpdated: false, daoImgUrl: '', pdfCreated: false, pdfDeleted: false, daoPdf: [] }

export const dao = {

    namespaced: true,
    state: initialState,
    getters: {
        dao: (state) => state.dao,
        daoName: (state) => state.daoName
    },
    mutations: {
        setDao(state) {
            let dao = JSON.parse(localStorage.getItem('dao'));
            state.dao = dao;
        },
        setDaoName(state, daoName) {
            state.daoName = daoName
        },
        deleteDao(state) {
            state.dao = null,
            state.daoName = ""
        },
        daoUpdated(state, url) {
            state.daoUpdated = true;
            state.dao.url = url;
            let dao = JSON.parse(localStorage.getItem('dao'));
            dao.url = url;
            localStorage.setItem('dao', JSON.stringify(dao));
        },
        pdfCreated(state, pdf) {
            state.pdfCreated = true;
            state.dao.daoPdf.push(pdf);
            let dao = JSON.parse(localStorage.getItem('dao'));
            dao.daoPdf.push(pdf);
            localStorage.setItem('dao', JSON.stringify(dao));
        },
        pdfDeleted(state, pdfUrl) {
            for (let i = 0; i < state.dao.daoPdf.length; i++) {
                if (state.dao.daoPdf[i].url === pdfUrl) {
                    state.dao.daoPdf.splice(i, 1);
                    let dao = JSON.parse(localStorage.getItem('dao'));
                    dao.daoPdf.splice(i, 1);
                    localStorage.setItem('dao', JSON.stringify(dao));
                }
            }
        }
    },
    actions: {
        async deleteDao({ commit }) {
            const data = await DaoService.delete();
            if (data) {
                commit("deleteDao");
                return true;
            }
        },
        async updateImage({ commit }, files) {
            try {
                const res = await daoService.updateImage(files);
                if (res) {
                    commit("daoUpdated", res.path);
                    return res;
                }
            }
            catch (err) {
                console.log(err);
            }

        },
        async updateDaoName({ commit }, payload) {
            try {
                const res = await daoService.updateDaoName(payload);
                if (res) {
                    commit("daoUpdated");
                    let dao = JSON.parse(localStorage.getItem('dao'));
                    dao.name = res.name;
                    localStorage.setItem('dao', JSON.stringify(dao));
                    return res;
                }
            } catch (err) {
                console.log(err);
            }
        },
        async createPdf({ commit }, payload) {
            try {
                const res = await daoService.createPdf(payload);
                if (res) {
                    commit('pdfCreated', res.pdf);
                    return res
                }
            } catch (err) {
                console.log(err.message);
            }
        },
        async deletePdf({ commit }, payload) {
            try {
                const res = await daoService.deletePdf(payload);
                if (res) {
                    commit('pdfDeleted', payload.value);
                    console.log(res);
                    return res;
                }
            } catch (err) {
                console.log(err.message)
            }
        }
    }
}