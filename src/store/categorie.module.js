import CatService from "../services/categories.service";
import txService from '../services/tx.service';

export const categories = {
    namespaced: true,
    state: {
        categories: [],
        txAdded: false,
        txFetched: false,
        txForCat: [],
        txDeleted: false
    },
    getters: {
        getCategories: (state) => (id) => {
            return state.categories.find(categorie => {
                categorie.walletId === parseInt(id)
            })
        },
        getTxByCategories: (state) => (id) => {
            let txCat = []
            state.txForCat.find(tx => {
                if(tx.categoryId === parseInt(id)) {
                    txCat.push(tx);
                }
            })
            return txCat;
        }
    },
    mutations: {
        setCategories(state, categorie) {
            state.categories.push(categorie);
        },
        deleteCat(state) {
            state.categories = [];
        },
        deleteCatById(state, catId) {
            for (let i = 0; i < state.categories.length; i++) {
                if (state.categories[i].id === catId) {
                  state.categories.splice(i, 1);
                }
              }
        },
        deleteTxForCat(state) {
            state.txForCat = [];
        },
        txAddedToCat(state) {
            state.txAdded = true
        },
        txFetched(state, tx){
            state.txFetched = true;
            state.txForCat.push(tx);
        },
        txDeleted(state, txId) {
            state.txDeleted = true;
            state.txForCat = state.txForCat.filter((tx) => {
                return tx.id !== txId;
            })
        }

    },
    actions: {
        async createCategorie({ commit }, payload) {
            console.log(payload);
            const res = await CatService.create(payload);
            if (res) {
                commit("setCategories", res.categorie);
            }
        },
        async deleteCategories({ commit }, payload) {
            const res = await CatService.deleteCategories(payload);
            if (res) commit('deleteCatById', payload);
        },
        async getCategories({ commit }) {
            commit('deleteCat');
            const res = await CatService.getCategories();
            if (res) {
                res.categories.forEach(e => {
                    commit("setCategories", e)
                });
                return await Promise.resolve(res.categories);
            }
        },
        async getTxByCategories({ commit }, payload) {
            commit("deleteTxForCat");
            const user = JSON.parse(localStorage.getItem('user'));
            if (user) {
                const res = await txService.getTxByCategories(payload, user.id);
                if(res) {
                    res.transactions.forEach(tx => {
                        commit('txFetched', tx);
                    })
                    return res;
                }
            }
        }, 

        async addTxToCategories({ commit },data) {
            const user = JSON.parse(localStorage.getItem('user'));
            if (user) {
                const res = await txService.addTxToCategories(data);
                if(res) {
                    commit('txAddedToCat');
                    return res;
                }
            }
        },
        async deleteTx({ commit }, payload) {
            try {
                const res = await txService.deleteTx(payload);
                if (res) {
                    commit('txDeleted', payload.txId);
                    return res
                }
            } 
            catch(err) {
                console.log(err.message);
            }
        }
    }
}