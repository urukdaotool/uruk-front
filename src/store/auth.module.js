import AuthService from '../services/auth.service';
const user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? { status: { loggedIn: true }, user } : { status: { loggedIn: false }, user: null };

export const auth = {
  namespaced: true,
  state: initialState,
  mutations: {
    loginSuccess(state, user) {
      state.status.loggedIn = true;
      state.user = user;
    },
    loginFailure(state) {
      state.status.loggedIn = false;
      state.user = null;
    },
    logout(state) {
      state.status.loggedIn = false;
      state.user = null;
      localStorage.removeItem("dao");
      localStorage.removeItem("user");
    },
    registerSuccess(state, user) {
      state.status.loggedIn = false;
      state.user = user;
    },
    registerFailure(state) {
      state.status.loggedIn = false;
    }
  },
  actions: {
    async login({ commit }, address) {
      const data  = await AuthService.login(address);
      if (!data) {
        commit('loginFailure');
        return false;
      }
      commit('loginSuccess', data);
      return Promise.resolve(data);
    },

    async logOut({ commit }) {
        AuthService.logout();
        commit('logout');
    },
    register({ commit }, user) {
      
      return AuthService.register(user).then(
        response => {
          commit('registerSuccess', response);
          return Promise.resolve(response);
        },
        error => {
          commit('registerFailure');
          return Promise.reject(error);
        }
      );
    }
  }
};
