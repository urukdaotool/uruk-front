import { createStore } from 'vuex';
import { auth } from "./auth.module";
import { tx } from "./tx.module";
import { dao } from "./dao.module";
import { categories } from "./categorie.module";
import { wallet } from "./wallet.module";
import { collection } from './collection.module';
import { utils } from './utils.module';

const store = createStore({
  modules: {
    auth,
    tx,
    dao,
    categories,
    wallet,
    collection,
    utils
  },
});
export default store;