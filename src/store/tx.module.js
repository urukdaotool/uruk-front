import axios from 'axios';
import authHeader from '../services/auth-header';


export const tx = {
  namespaced: true,
  state: {
    transactionsData: []
  },
  getters: {
    transactionData: (state) => state.transactionsData,
    walletAddress: (state) => state.walletAddress
  },
  mutations: {
    setTransactionsData(state, transactionsData) {
      state.transactionsData = transactionsData;
    },
    deleteTxArray(state) {
      state.transactionsData = [];
    }
  },
  actions: {
    async fetchTransactionsData(context, payload) {
      const user = JSON.parse(localStorage.getItem('user'));

      context.commit("deleteTxArray");
      
      if (user) {
        const transactions = await axios.post(process.env.VUE_APP_API_URL + "transactions", { 
          address: payload, 
          userId: user.id 
        }, { headers: authHeader() });

        context.commit("setTransactionsData", transactions.data.transactions);
      }
    }
  }
}