import axios from 'axios';
import authHeader from '../services/auth-header';

class TxService {
    async addTxToCategories(payload) {
        const user = JSON.parse(localStorage.getItem("user"));
        try {
            const { data } = await axios
                .post(process.env.VUE_APP_API_URL + "create/transactions", {
                    userId: user.id,
                    catId: payload.catId,
                    tx: payload.tx,
                    wallet: payload.wallet
                }, { headers: authHeader() })
            return data
        } catch (err) {
            console.log(err)
            return err.message
        }

    }
    async getTxByCategories(payload, userId) {
        const { data } = await axios.get(process.env.VUE_APP_API_URL + "transactions", {
            params: {
                userId: userId,
                catId: payload
            },
            headers: authHeader()
        })
        return data;
    }
    async deleteTx(payload) {
        const user = JSON.parse(localStorage.getItem("user"));
        try {
            const { data } = await axios.post(process.env.VUE_APP_API_URL + "delete/transactions", {
                userId: user.id,
                txId: payload.txId,
                catId: payload.catId
            }, { headers: authHeader() })
            return data;
        }
        catch (err) {
            return err.message;
        }

    }
}

export default new TxService();