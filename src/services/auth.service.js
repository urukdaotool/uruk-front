import axios from 'axios';
import authHeader from '../services/auth-header';

class AuthService {
  async login(address) {
    try {
      const { data } = await axios.post(process.env.VUE_APP_API_URL + 'auth/signin', {
        address: address
      })

      if (data.accessToken) {
        localStorage.setItem('user', JSON.stringify(data));
        data.daos.forEach(daoUser => {
          localStorage.setItem('dao', JSON.stringify(daoUser));
          let dao = JSON.parse(localStorage.getItem('dao'));
          dao.url = data.image[0]?.url;
          dao.daoPdf = data.pdfs
          localStorage.setItem('dao', JSON.stringify(dao));
        });
        return data;
      }

    } catch (err) {
      console.log(err.message);
      return false;
    }
  }

  async logout() {
    const user = JSON.parse(localStorage.getItem('user'));

    await axios.post(process.env.VUE_APP_API_URL + 'auth/logout', {
      userId: user.id
    }, { headers: authHeader() });

    localStorage.removeItem('user');
    localStorage.removeItem('dao');
  }

  async register(user) {

    if (user.daoOwner === undefined) {
      user.daoOwner = "user"
    }
    const { data } = await axios.post(process.env.VUE_APP_API_URL + 'auth/signup', {
      address: user.address,
      daoName: user.daoName,
      helloMoonCollectionId: user.helloMoonCollectionId,
      roles: [user.daoOwner]
    });
    if (data) {
      if (data.accessToken) {
        localStorage.setItem('user', JSON.stringify(data));
        data.daos.forEach(dao => {
          localStorage.setItem('dao', JSON.stringify(dao));
        });
      }
      return data;
    }
  }
}
export default new AuthService();
