export default function authHeader() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.accessToken) {
      const header = {
        "Content-Type": "application/json",
        "Accept": "*/*",
        'x-access-token': user.accessToken
      }
      return header;
    } else {
      return {};
    }
  }
  