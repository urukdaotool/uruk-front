import axios from 'axios';
import authHeader from '../services/auth-header';

class CollectionService {
    async getCollection() {
        const dao = JSON.parse(localStorage.getItem('dao'));
        const user = JSON.parse(localStorage.getItem('user'));
        try {
            const { data } = await axios.get(process.env.VUE_APP_API_URL + "collection", {
                params: {
                    daoId: dao.id,
                    userId: user.id
                },
                headers: authHeader()
            })
            if(data) {
                return data;
            }
        } catch(err) {
            if(err.message) return err.message;
            if(err.status === 404) return err.message
        }

    }
}

export default new CollectionService();